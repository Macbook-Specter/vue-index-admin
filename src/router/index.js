import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'


/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/',
    component: () => import('@/views/home/index'),
    hidden: true
  },
  {
    path: '/text',
    component: () => import('@/views/article/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/admin',
    component: Layout,
    redirect: '/article',
    children: [
      {
      path: 'article',
      name: 'Article',
      component: () => import('@/views/admin/contentmanagement/article'),
      meta: { title: '文章管理', icon: 'nested' }
      },
      {
        path: 'comments',
        name: 'Comments',
        component: () => import('@/views/admin/contentmanagement/comment'),
        meta: { title: '评论管理', icon: 'dashboard' }
      }
    ]
  }
  

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/sysAdmin',
    component: Layout,
    redirect: '/sysAdmin',
    name: '系统管理',
    meta: {
      title: '系统管理',
      icon: 'nested',
      roles: ['ROLE_ADMIN', 'ROLE_ROOT']
    },
    children: [
      {
        path: 'user',
        component: () => import('@/views/admin/systemmanagement/user'),
        name: '用户管理',
        meta: { title: '用户管理' ,icon: 'user'}
      },
      {
        path: 'userGroud',
        component: () => import('@/views/admin/systemmanagement/userGroud'),
        name: '用户组管理',
        meta: { title: '用户组管理' }
      },
      {
        path: 'plan',
        component: () => import('@/views/admin/systemmanagement/Program'),
        name: '方案管理',
        meta: { title: '方案管理' }
      },
      {
        path: 'label',
        component: () => import('@/views/admin/systemmanagement/label'),
        name: '标签管理',
        meta: { title: '标签管理' }
      },
      {
        path: 'classification',
        component: () => import('@/views/admin/systemmanagement/classification'),
        name: '栏目管理',
        meta: { title: '栏目管理' }
      },
      {
        path: 'log',
        component: () => import('@/views/admin/systemmanagement/log'),
        name: '日志管理',
        meta: { title: '日志管理' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
