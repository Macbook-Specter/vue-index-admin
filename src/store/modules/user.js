import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    id: 0,
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    telephone: 0,
    email: '',
    nickname: '',
    usergroupid: 0
  }
}

const state = getDefaultState()

const mutations = {
  SET_ID: (state, id) => {
    state.id = id
  },
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_NICKNAME: (state, nickname) => {
    state.nickname = nickname
  },
  SET_TELEPHONE: (state, telephone) => {
    state.telephone = telephone
  },
  SET_EMAIL: ( state, email) => {
    state.email = email
  },
  SET_USERGROUPID: (state, usergroupid) =>{
    state.usergroupid = usergroupid
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const { authorization } = response.headers
        commit('SET_TOKEN', authorization)
        setToken(authorization)
        resolve()

      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {

        const { data } = response

        if (!data) {
          reject('没有获取到数据！')
        }
        const { id, roles, nickname, telephone, username, email, usergroupid } = data

        // roles must be a non-empty array
        if (!roles || roles.length <= 0) {
          reject('getInfo: roles must be a non-null array!')
        }
        commit('SET_ID', id)
        commit('SET_ROLES', roles)
        commit('SET_NAME', username)
        commit('SET_NICKNAME', nickname)
        commit('SET_TELEPHONE', telephone)
        commit('SET_EMAIL', email)
        commit('SET_USERGROUPID', usergroupid)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

