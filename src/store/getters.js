const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  name: state => state.user.name,
  nickname: state => state.user.nickname,
  email: state => state.user.email,
  roles: state => state.user.roles,
  userGroupId: state => state.user.usergroupid,
  userId: state => state.user.id,
  permission_routes: state => state.permission.routes
}
export default getters
