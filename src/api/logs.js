import request from '@/utils/request'


  export function query(data) {
    return request({
      url: '/admin/logs/query',
      method: 'post',
      data
    })
  }

  export function del(id) {
    return request({
      url: '/admin/logs/del',
      method: 'delete',
      params:{
        "id":id
    }
    })
  }