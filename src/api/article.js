import request from '@/utils/request'

export function query(data) {
    return request({
      url: '/admin/news/query',
      method: 'post',
      data
    })
  }
  export function save(data) {
    return request({
      url: '/admin/news/save',
      method: 'post',
      data
    })
  }
  export function edit(data) {
    return request({
      url: '/admin/news/edit',
      method: 'post',
      data
    })
  }
  export function del(id) {
    return request({
      url: '/admin/news/del',
      method: 'delete',
      params:{
          "id":id
      }
    })
  }
