import request from '@/utils/request'

export function generate(data) {
    return request({
      url: '/admin/classify/generate',
      method: 'get',
      data
    })
  }
  export function query(data) {
    return request({
      url: '/admin/classify/query',
      method: 'post',
      data
    })
  }
  
  export function del(id) {
    return request({
      url: '/admin/classify/del',
      method: 'delete',
      params:{
        "id":id
    }
    })
  }

  export function getClassifyList() {
    return request({
      url: '/getClassifyList',
      method: 'get'
    })
  }