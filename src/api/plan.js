import request from '@/utils/request'

export function save(data) {
    return request({
      url: '/admin/paln/save',
      method: 'post',
      data
    })
  }
  export function query(data) {
    return request({
      url: '/admin/paln/query',
      method: 'post',
      data
    })
  }
  export function update(data) {
    return request({
      url: '/admin/paln/update',
      method: 'post',
      data
    })
  }
  export function del(id) {
    return request({
      url: '/admin/paln/del',
      method: 'delete',
      params:{
        "id":id
    }
    })
  }
