import request from '@/utils/request'

export async function addReadRecord(data) {
  return request({
    url: '/addReadRecord',
    method: 'post',
    data
  })
}

export async function addLike(id){
  return request({
    url: '/addLike',
    method: 'get',
    params: {
      'newsId': id
    }
  })
}

export function  getOneNews(id){
  return request({
    url: '/oneNews',
    method: 'get',
    params: {
      id
    }
  })
}
