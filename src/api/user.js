import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data: {
      'name' :data.username,
      'password' : data.password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/userinfo',
    method: 'get',
    params: { 'token':token }
  })
}
// export function getInfo(token) {
//   return request({
//     url: '/userinfo',
//     method: 'get'
//   })
// }
export function logout(token) {
  return request({
    url: '/out',
    method: 'post',
    data: {
      'token' : token
    }
  })
}
export function page(data) {
  return request({
    url: '/admin/user/page',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/admin/user/save',
    method: 'post',
    data
  })
}
export function update(data) {
  return request({
    url: '/admin/user/update',
    method: 'post',
    data
  })
}
export function del(id) {
  return request({
    url: '/admin/user/del',
    method: 'delete',
    params:{
      "id":id
  }
  }
  )
}

export function getRoleList() {
  return request({
    url: '/getRoleList',
    method: 'get'
  }
  )
}

