import request from '@/utils/request'

export function save(data) {
    return request({
      url: '/admin/group/save',
      method: 'post',
      data
    })
  }
  export function query(data) {
    return request({
      url: '/admin/group/query',
      method: 'post',
      data
    })
  }
  export function update(data) {
    return request({
      url: '/admin/group/update',
      method: 'post',
      data
    })
  }
  export function del(id) {
    return request({
      url: '/admin/group/del',
      method: 'delete',
      params:{
        "id":id
    }
    })
  }
  export function getGroupList() {
    return request({
      url: '/getGroupList',
      method: 'get'
    }
    )
  }
