import request from '@/utils/request'
export function query(data) {
  return request({
    url: '/admin/newscomments/query',
    method: 'post',
    data
  })
}
export function addComments(data) {
  return request({
    url: '/addComments',
    method: 'post',
    data
  })
}
export function del(id) {
  return request({
    url: '/admin/newscomments/del',
    method: 'delete',
    params: {
      'id': id
    }
  })
}
