import request from '@/utils/request'

export function save(data) {
    return request({
      url: '/admin/tag/save',
      method: 'post',
      data
    })
  }
  export function query(data) {
    return request({
      url: '/admin/tag/query',
      method: 'post',
      data
    })
  }
  export function update(data) {
    return request({
      url: '/admin/tag/update',
      method: 'post',
      data
    })
  }
  export function del(id) {
    return request({
      url: '/admin/tag/del',
      method: 'delete',
      params:{
        "id":id
    }
    })
  }